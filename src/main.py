#!/usr/bin/env python3.11

import pathlib, tomllib

from flask import Flask, request, render_template, redirect, flash
from werkzeug.utils import secure_filename

with open("config.toml", "rb") as f:
    cfg = tomllib.load(f)

app = Flask(__name__)
app.config["UPLOAD_FOLDER"] = cfg["upload_folder"]
app.config["MAX_CONTENT_LENGTH"] = cfg["max_content_length"] * 1000 * 1000  # bytes
app.config["SECRET_KEY"] = cfg["secret_key"]


@app.route("/", methods=["POST", "GET"])
def index():
    match request.method:
        case "POST":
            if len(request.files) == 0:
                flash("No files submitted")
                return redirect(request.url)

            fieldname = "upload"

            if fieldname not in request.files:
                flash(f"No file named '{fieldname}'")
                return redirect(request.url)

            file = request.files[fieldname]

            if file.filename == "":
                flash("No selected file")
                return redirect(request.url)

            filename = secure_filename(file.filename)
            path = pathlib.Path(app.config["UPLOAD_FOLDER"]) / filename
            file.save(path)
            flash(f"File successfully uploaded to {path}")

            return redirect(request.url)

        case "GET":
            return render_template("index.html")
