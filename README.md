# upload

A simple web page for upload files to a server.

## Configuration

Initialise with

```sh
cp template.config.toml config.toml
```

and set fields.

Generate secret key with, for example,

```sh
cat /dev/urandom | base64 | head -c 16 && echo
```

Install dependencies with

```sh
pdm install
```

## Run

```sh
pdm start
```

## Dev

```sh
pdm dev
```

## Example nginx proxy server

Serve `pdm start` without TLS on LAN.
Use a subdomain on the host (LAN nameserver must have appropriate CNAME record).

`/etc/nginx/sites-available/upload`

```nginx
server {
    listen 80;

    server_name upload.<hostname>.lan;

    client_max_body_size 100m;

    location / {
        proxy_pass http://127.0.0.1:5001/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

## Example systemd profile

So server starts when machine starts after `sudo systemctl enable upload`.

`/etc/systemd/system/upload.service`

```systemd
[Unit]
Description=Upload portal
After=network.target

[Service]
User=<user>
Group=<user>

WorkingDirectory=/home/<user>/git/upload
ExecStart=/home/<user>/.local/bin/pdm start

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target

```
